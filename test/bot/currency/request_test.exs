defmodule Bot.Currency.RequestTest do
  use ExUnit.Case

  @params %{
    "body" => "4 USD in EUR",
    "inserted_at" => "2017-08-30T04:40:35.022442Z",
    "token" => "abcdef",
    "response_url" => "https://google.com",
    "metadata" => %{"ref" => 1}
  }

  @request %Bot.Currency.Request{
    amount: 4.0,
    base: "USD",
    symbols: "EUR",
    token: "abcdef",
    response_url: "https://google.com",
    metadata: %{"ref" => 1},
    inserted_at: "2017-08-30T04:40:35.022442Z"
  }

  test "Conversion from params" do
    assert Bot.Currency.Request.from_params(@params) == @request
  end

  test "Conversion from params with multiple symbols" do
    params = %{@params | "body" => "4 USD in EUR, JPY, RUB"}
    request = %{@request | symbols: "EUR,JPY,RUB"}
    assert Bot.Currency.Request.from_params(params) == request
  end

  test "Conversion from params with different characters case in body" do
    params = %{@params | "body" => "4 Usd in Eur, jPY, rUb"}
    request = %{@request | symbols: "EUR,JPY,RUB"}
    assert Bot.Currency.Request.from_params(params) == request
  end

  test "Conversion from params with integer as amount converts to valid float" do
    params = %{@params | "body" => "123 USD in EUR"}
    request = %{@request | amount: 123.0}
    assert Bot.Currency.Request.from_params(params) == request
  end

  test "Conversion from params with float amount converts to valid float" do
    params = %{@params | "body" => "123.45 USD in EUR"}
    request = %{@request | amount: 123.45}
    assert Bot.Currency.Request.from_params(params) == request
  end
end

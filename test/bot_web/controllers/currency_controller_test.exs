defmodule BotWeb.CurrencyControllerTest do
  use BotWeb.ConnCase

  @request %{
    "body" => "4 USD in EUR",
    "inserted_at" => "2017-08-30T04:40:35.022442Z",
    "token" => "0123456789abcdef",
    "response_url" => "http://localhost:4001/api/test",
    "metadata" => %{}
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "POST /api/currency" do
    test "Response is valid", %{conn: conn} do
      pid = self()
            |> :erlang.pid_to_list()
            |> to_string()
      request = %{@request | "metadata" => %{"pid" => pid}}
      conn = post conn, currency_path(conn, :get), request
      assert json_response(conn, 202) == %{}
    end

    test "Valid result is sent after processing", %{conn: conn} do
      pid = self()
            |> :erlang.pid_to_list()
            |> to_string()
      request = %{@request | "metadata" => %{"pid" => pid}}
      post conn, currency_path(conn, :get), request
      assert_receive {:reply, message}, 2000
      assert message["body"] =~ " EUR"
      assert message["token"] == request["token"]
    end

    test "Multiple results are sent after processing", %{conn: conn} do
      pid = self()
            |> :erlang.pid_to_list()
            |> to_string()
      request = @request
                |> Map.put("metadata", %{"pid" => pid})
                |> Map.put("body", "45 USD in EUR, JPY")
      post conn, currency_path(conn, :get), request
      assert_receive {:reply, message1}, 2000
      assert_receive {:reply, message2}, 2000
      assert message1["token"] == request["token"]
      assert message1["body"] <> message2["body"] =~ " EUR"
      assert message1["body"] <> message2["body"] =~ " JPY"
    end

    test "Result is human-friendly", %{conn: conn} do
      pid = self()
            |> :erlang.pid_to_list()
            |> to_string()
      request = %{@request |
        "metadata" => %{"pid" => pid},
        # Mock currency rates source returns rates between 1 and 2, just a big
        # number to convert will be enough for the test
        "body" => "100000000 USD in JPY"
      }
      post conn, currency_path(conn, :get), request
      assert_receive {:reply, message}, 2000
      assert message["body"] =~ ~r/[\d]*\.[\d]{1,2}\sJPY/
    end

    test "Respond with error to malformed requests", %{conn: conn} do
      pid = self()
            |> :erlang.pid_to_list()
            |> to_string()
      request = @request
                |> Map.delete("token")
                |> Map.put("metadata", %{"pid" => pid})
      conn = post conn, currency_path(conn, :get), request
      assert json_response(conn, 400) == %{"error" => "Malformed request"}
      refute_receive {:reply, _message}, 2000
    end
  end
end

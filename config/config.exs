# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :bot, BotWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Mq7Dpf8oUY9q9RlSAewMADlSt0LzyskWg+VE5/epe3DBYXTaWP/b9suawbz0LK5a",
  render_errors: [view: BotWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Bot.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :bot,
  env: :undefined,
  currency: [
    source: Bot.Currency.Source.Fixer
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

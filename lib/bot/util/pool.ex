defmodule Bot.Util.Pool do
  @moduledoc """
  Helper for defining functions to be run against Poolboy pools
  Functions defined within `defpool` macro will be automatically run
  inside pool transaction
  """

  defmacro __using__(opts) do
    pool = Keyword.fetch!(opts, :pool)
    quote do
      import unquote(__MODULE__)
      Module.put_attribute(__MODULE__, :pool, unquote(pool))
    end
  end

  @doc """
  ```
  defpool somefunction(arg1, arg2) do
    # `pid` parameter is implicitly added into the `do` block
    do_job(pid)
  end
  ```
  will be converted into
  ```
  def somefunction(arg1, arg2) do
    :poolboy.transaction(@pool, fn pid ->
      do_job(pid)
    end)
  end
  ```
  Also, parameter `pid` is implicitly passed to macro's `do` block
  """
  defmacro defpool({function, _meta, args}, do: block) do
    quote bind_quoted: [
      function: function,
      args: Macro.escape(args, unquote: true),
      block: Macro.escape(block, unquote: true)
    ] do
      def unquote(function)(unquote_splicing(args)) do
        :poolboy.transaction(@pool, fn pid ->
          var!(pid) = pid
          unquote(block)
        end)
      end
    end
  end
end

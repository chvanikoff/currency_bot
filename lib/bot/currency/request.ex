defmodule Bot.Currency.Request do
  @moduledoc """
  Internal representation of request to currency bot
  """

  defstruct [
    :amount,
    :base,
    :symbols,
    :token,
    :response_url,
    :metadata,
    :inserted_at
  ]

  @type t :: %__MODULE__{
    amount: number,
    base: String.t,
    symbols: String.t,
    token: String.t,
    response_url: String.t,
    metadata: String.t,
    inserted_at: String.t
  }

  @doc """
  Converts params sent to CurrencyController via HTTP
  into %Bot.Currency.Request{} struct

  ## Examples
      
      iex> from_params(%{
        "body" => "4 USD in EUR, JPY",
        "inserted_at" => "2017-08-30T04:40:35.022442Z",
        "token" => "abcdef",
        "response_url" => "https://google.com",
        "metadata" => %{"ref" => 1}
      })
      %Bot.Currency.Request{
        amount: 4.0,
        base: "USD",
        symbols: "EUR, JPY",
        token: "abcdef",
        response_url: "https://google.com",
        metadata: %{"ref" => 1},
        inserted_at: "2017-08-30T04:40:35.022442Z"
      }

  """
  @spec from_params(Map.t) :: __MODULE__.t
  def from_params(params) do
    r_amount = "[\\d]*\.?[\\d]+"
    r_base = "[a-z]{3}"
    r_symbols = "[a-z,\\s]*"
    regex = ~r/(#{r_amount})\s(#{r_base})\sin\s(#{r_symbols})/i
    [_, amount, base, symbols] = Regex.run(regex, Map.fetch!(params, "body"))
    {amount, ""} = Float.parse(amount)
    symbols = symbols
              |> String.replace(" ", "")
              |> String.upcase()
    %__MODULE__{
      amount: amount,
      base: String.upcase(base),
      symbols: symbols,
      token: Map.fetch!(params, "token"),
      response_url: Map.fetch!(params, "response_url"),
      metadata: Map.fetch!(params, "metadata"),
      inserted_at: Map.fetch!(params, "inserted_at")
    }
  end
end

defmodule Bot.Currency.Worker do
  @moduledoc """
  Currency bot worker.
  Responsible for processing requests and sending replies.
  """

  use Bot.Util.Pool, pool: Bot.Currency.Pool

  require Logger

  defpool request(req) do
    GenServer.cast(pid, {:request, req})
  end

  defpool reply(req, message) do
    GenServer.cast(pid, {:reply, req, message})
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, [])
  end

  def init(_args) do
    source = Application.get_env(:bot, :currency)[:source]
    {:ok, %{source: source}}
  end

  @doc """
  Process request
  """
  def handle_cast({:request, req}, %{source: source} = state) do
    source.get_rates(req.base, req.symbols)
    |> Enum.each(fn {symbol, rate} ->
      converted = req.amount * rate
      # Result can be anything like
      # 8302.199999999999
      # or 1.1328e6
      # which is not very human-friendly. That's why we format how the float
      # result will be sent to client
      formated= :io_lib.format("~.2f", [converted])
                |> IO.iodata_to_binary()
      message = "#{formated} #{symbol}"
      reply(req, message)
    end)
    {:noreply, state}
  end

  @doc """
  Process replies

  When running tests, there's no way other than mocking to ensure valid response have been sent.
  The approach of passing running test's pid in "metadata" and sending message to it seems to be
  a better and cleaner one.
  """
  def handle_cast({:reply, req, message}, state) do
    body = %{
      "body" => message,
      "token" => req.token
    }
    if Bot.env() == :test do
      pid = req.metadata["pid"]
            |> String.to_charlist()
            |> :erlang.list_to_pid()
      send(pid, {:reply, body})
    end
    result = HTTPoison.post(req.response_url, Poison.encode!(body), [{"Content-Type", "application/json"}])
    case result do
      {:ok, %HTTPoison.Response{status_code: 204, body: ""}} ->
        :ok
      other ->
        Logger.error "Got unexpected response from #{req.response_url}: #{inspect other}"
    end
    {:noreply, state}
  end
end

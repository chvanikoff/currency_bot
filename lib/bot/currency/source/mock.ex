defmodule Bot.Currency.Source.Mock do
  @moduledoc """
  Mock currency rates source
  Goal is to not depend on network when running tests
  """

  @behaviour Bot.Currency.Source

  @impl Bot.Currency.Source
  @spec get_rates(String.t, String.t) :: [{String.t, float}]
  def get_rates(_base, symbols) do
    symbols
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> Enum.map(fn symbol ->
      # rate is float between 1 and 2
      rate = 1 + Float.round(:rand.uniform(), 5)
      {symbol, rate}
    end)
  end
end

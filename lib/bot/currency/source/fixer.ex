defmodule Bot.Currency.Source.Fixer do
  @moduledoc """
  Currency rates source
  Fetches rates from Fixer.io API via HTTP
  """

  @behaviour Bot.Currency.Source

  require Logger

  @endpoint "https://api.fixer.io"

  @impl Bot.Currency.Source
  @spec get_rates(String.t, String.t) :: [{String.t, float}]
  def get_rates(base, symbols) do
    url = "#{@endpoint}/latest?base=#{base}&symbols=#{symbols}"
    result = HTTPoison.get(url)
    case result do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        %{"rates" => rates} = Poison.decode!(body)
        Enum.into(rates, [])
      other ->
        Logger.error "Got error requesting Fixer API: #{inspect other}"
    end
  end
end

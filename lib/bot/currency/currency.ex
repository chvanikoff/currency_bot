defmodule Bot.Currency do
  @moduledoc """
  Currency bot API
  """

  alias Bot.Currency

  @doc """
  Runs the Currency bot.
  When called, a request is build from params and put into queue 
  """
  @spec run(Map.t) :: :ok
  def run(params) do
    :ok = params
          |> Currency.Request.from_params()
          |> Currency.Worker.request()
    :ok
  end
end

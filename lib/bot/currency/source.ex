defmodule Bot.Currency.Source do
  @moduledoc """
  Behaviour of currency source modules
  """

  @callback get_rates(String.t, String.t) :: [{String.t, float}]
end

defmodule Bot do
  @moduledoc """
  Bot keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @doc """
  Mix.env() is unavailable when running app in release, so we define env
  in each config file
  """
  @spec env() :: :undefined | :dev | :test | :prod
  def env() do
    Application.get_env(:bot, :env, :undefined)
  end
end

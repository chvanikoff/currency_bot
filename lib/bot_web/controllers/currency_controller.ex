defmodule BotWeb.CurrencyController do
  use BotWeb, :controller

  def get(conn, %{"body" => _, "inserted_at" => _, "token" => _, "response_url" => _, "metadata" => _} = params) do
    :ok = Bot.Currency.run(params)
    conn
    |> put_status(202)
    |> json(%{})
  end
  def get(conn, _params) do
    conn
    |> put_status(400)
    |> json(%{"error" => "Malformed request"})
  end

  def test(conn, _params) do
    conn
    |> put_status(204)
    |> text("")
  end
end

defmodule BotWeb.Router do
  use BotWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BotWeb do
    pipe_through :api

    post "/currency", CurrencyController, :get

    if Bot.env() == :test do
      post "/test", CurrencyController, :test
    end
  end
end

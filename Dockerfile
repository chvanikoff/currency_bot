FROM bitwalker/alpine-elixir-phoenix:1.5.2

ENV MIX_ENV=prod \
    REPLACE_OS_VARS=true

RUN mkdir build
WORKDIR build

COPY mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

COPY . ./

RUN mix compile

ARG VERSION

RUN mix release --env=prod \
  && cp _build/prod/rel/bot/releases/${VERSION}/bot.tar.gz ../

WORKDIR ../

RUN tar -xf bot.tar.gz \
  && rm -rf build bot.tar.gz

USER default

ENTRYPOINT ["/opt/app/bin/bot"]
CMD ["foreground"]

